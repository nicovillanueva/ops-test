package main

import "fmt"

func sayHello() string {
	return "hello"
}

func main() {
	fmt.Printf("%s world\n", sayHello())
}
