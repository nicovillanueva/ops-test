package main

import "testing"

func Test_sayHello(t *testing.T) {
	if sayHello() != "hello" {
		t.Fail()
	}
}
